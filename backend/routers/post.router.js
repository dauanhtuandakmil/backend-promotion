const Router = require('express').Router();
const postController = require('.././controllers/posts.controller');

Router.post('/createPost',createPost);
Router.put('/updatePost',updatePost);
Router.delete('/deletePost',deletePost);
Router.get('/getAllPost',getAllPost);
Router.post('/updateImagePost/:id',updateImgaePost)
module.exports = Router;
function getAllPost(req,res,next){
    postController.getAllPost()
    .then((data)=>{
        return res.send(data);
    })
    .catch((err)=>{
        return next(err);
    })
}
function updatePost(req,res,next){
    var id = req.params.id;
    var data = req.data;
    postController.updatePost(id,data)
    .then((post)=>{
        return res.send({message: "success"})
    })
    .catch((err)=>{
        return res.send({message:"fail"});
    })
}
function deletePost(req,res,next){
    var id = req.params.id;
    postController.deletePost(id)
    .then(()=>{
        return res.send({message:"success"})
    })
    .catch((err)=>{
        return res.send({message:"fail"})
    })
}
function createPost(req,res,next){
    var body = req.body;
    postController.createPost(body)
    .then((data)=>{
        return res.send(data);
    })
    .catch((err)=>{
        return next(err);
    })
}
function updateImgaePost(req,res,next){
    var id = req.params.id;
    console.log(id);
    
    if(!req.files){
        return res.status(400).send('NO files were uploaded');
    }
    let file = req.files.file;
    console.log();
    
    postController.updateImagePost(id,file)
    .then((image) =>{
        return res.send({
            image : image
        })
    })
    .catch((err)=>{
        return res.send('Fail');
    })
}