const Router = require('express').Router();
const recruiterController = require('./../controllers/recruiter.controller');

Router.get('/getRecruiter', getAll);
Router.post('/createRecruiter',createRecruiter);
Router.put('/updateRecruiter/:id',updateRecruiter);
Router.delete('/deleteRecruiter/:id',deleteRecruiter);
module.exports = Router;

function getAll(req,res,next){
    recruiterController.getAllRecuiter()
    .then((res)=>{
        return res.send(res);
    })
    .catch((err)=>{
        return next(err);
    })
}
function createRecruiter(req,res,next){
    var body = req.body;
    recruiterController.createRecruiter(body)
    .then((data)=>{
        return res.send(data);
    })
    .catch((err)=>{
        return next(err);
    })
}
function deleteRecruiter(req,res,next){
    var id = req.params.id;
    recruiterController.deleteRecruiter(id)
    .then((res)=>{
        return res.send({message: "success"})
    })
    .catch((err)=>{
        return res.send({message:"fail"})
    })
}
function updateRecruiter(req,res,next){
    var id = req.params.id;
    var data = req.body;
    recruiterController.updateRecuiter(id,data)
    .then((recuiter)=>{
        res.send({message:"success"})
    })
    .catch((err)=>{
        return res.send({message:"fail"})
    })
}