const Router = require('express').Router();
const candidateController = require('./../controllers/candidates.controller');

Router.get('/getCandidate',getCandidate);
Router.post('/createCandidate',createCandidate);
Router.put('/updateCandidate/:id', updateCandidate);
Router.delete('/deleteCandidate/:id',deleteCandidate);
Router.post('/UploadImgCandidate/:id',updateImgaeCandidate)
module.exports = Router;
function createCandidate(req,res,next){
    var body = req.body;
    candidateController.addCandidate(body)
    .then((data)=>{
        return res.send(data);
    })
    .catch((err)=>{
        return next(err);
    })
}
function getCandidate(req,res,next){
    candidateController.getAllCandidate()
    .then((data)=>{
        return res.send(data);
    })
    .catch((err)=>{
        return next(err);
    })
};
function deleteCandidate(req,res,next){
    var id = req.params.id;
    candidateController.deleteCandidate(id)
    .then(()=>{
        return res.send({message:"success"});
    })
    .catch((err)=>{
        return res.send({message:"fail"})
    })
}
function updateCandidate(req,res,next){
    var id = res.params.id;
    var update = req.body;
    candidateController.updateCandidate(id,update)
    .then((candidate)=>{
        return res.send({message: "success"});
    })
    .catch((err)=>{
        return res.send({message:"fail"});
    })
}
function updateImgaeCandidate(req,res,next){
    var id = req.params.id;
    console.log(id);
    
    if(!req.files){
        return res.status(400).send('NO files were uploaded');
    }
    let file = req.files.file;
    console.log();
    
    candidateController.updateImageCandidate(id,file)
    .then((image) =>{
        return res.send({
            image : image
        })
    })
    .catch((err)=>{
        return res.send('Fail');
    })
}