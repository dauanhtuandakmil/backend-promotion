var mongoosse = require('mongoose');
var Schema = mongoosse.Schema;
var paramater = require('../constants/paramater');
var RecruiterSchema = new Schema({
    companyname:{
        type: String,
        required: true
    },
    address:{
        type: String,
        required:true
    },
    city:{
        type: String,
        required:true
    },
    taxcode:{
        type: String,
        required:true
    },
    email:{
        type: String,
        required:true    
    },
    phonenumber:{
        type: String,
    },
    images:[{
        type:String,
        default:"unknow"
    }],
    desc:{
        type: String,
        default:'unknow'
    },
    country: {
        type:String,
        required: true
    },
    statusAccept:{
        //0 accept 1 chua
        type:Number
    }
})
var recruiters = mongoosse.model('recruiters',RecruiterSchema)
module.exports = recruiters;