var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var paramater = require('../constants/paramater');
var PostSchema = new Schema({
    title:{
        type:String,
        required:true
    },
    desc:{
        type:String,
        required:true
    },
    time:{
        type:Date,
        required:true
    },
    salary:{
        type:Number,
        required:true
    },
    images:[
        {
            type: String,
            default:"unknow"
        }
    ],
    reason:{
        type: String,
        required: true,
    },
    require:{
        type: String,
        required: true,
    },
    address:{
        type:String,
        required: true
    },
    status:{
        type:Number,
        default: paramater.CANDIDATE_USE
    }
})
var posts = mongoose.model('posts',PostSchema);
module.exports = posts;