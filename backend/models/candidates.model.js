var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var paramater = require('../constants/paramater');
var CandidateSchema = new Schema({
    fullname:{
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    phonenumber:{
        type: String,
    },
    address: {
        type: String,
        required: true
    },
    sex: {
        type: Boolean
    },
    email: {
        type: String,
        required:true
    },
    images: [{
        type:String,
        default:"unknow"
    }],
    avatar: {
        type:String,
        default:"unknow"
    },
    CV: {
        type:String,
        default:"unknow"
    },
    description:{
        type: String,
        default:'unknow'
    },
    status:{
        //0 chua 1 roi
        type: Number,
        default: paramater.CANDIDATE_USE
    }
})
var candidates = mongoose.model('candidates',CandidateSchema);
module.exports = candidates;