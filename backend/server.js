const express      = require('express');
const app          = express();
const bodyParser   = require('body-parser');

const errorHandler = require('./middle-ware/error-handler');
const mongoose     = require('mongoose');
const UserRouter = require('./routers/user.router');
const fileUpload = require('express-fileupload');
const AuthRouter = require('./routers/auth.router');
const CandidateRouter = require('./routers/candidates.router');
const RecruiterRouter = require('./routers/recruiter.router');
const PostRouter = require('./routers/post.router');
const allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');
    next();
};
app.use(allowCrossDomain);
app.use(express.static('public')); // de public cho client co the su dung duoc file trong thu muc do
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



app.use('/api/User',UserRouter);
app.use('/api/Auth', AuthRouter);
app.use('/api/Candidate',CandidateRouter);
app.use('/api/Recruiter', RecruiterRouter);
app.use('/api/Post',PostRouter);
mongoose.connect('mongodb://localhost:27017/Promotion-job',(err)=>{
    if(err){
        console.log('not connect to the database');
    } else {
        console.log('Successfully connected to MongoDB')
    }
})
app.use(errorHandler.errorHandler());
app.listen(8088,(err)=>{
        console.log('server run port 8088 http://localhost:8088');
})