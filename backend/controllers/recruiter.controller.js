const recruiter = require('../models/recruiter.model');
const path = require('path');
const Status = require('../constants/status');
const uuid = require('uuid');
module.exports = {
    getAllRecuiter: getAllRecuiter,
    createRecruiter: createRecruiter,
    deleteRecruiter: deleteRecruiter,
    updateRecuiter: updateRecuiter
}
function getAllRecuiter(){
    return recruiter.find()
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function createRecruiter(body){
    let newRecruiter = new recruiter(body)
    return newRecruiter.save()
    .then((res)=>{
        return Promise.resolve(res)
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function deleteRecruiter(id){
    return recruiter.findByIdAndRemove(id)
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function updateRecuiter(id,data){
    return recruiter.findByIdAndUpdate({_id:id},data)
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}