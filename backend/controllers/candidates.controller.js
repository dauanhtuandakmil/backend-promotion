const candidate = require('../models/candidates.model');
const path = require('path');
const Status = require('../constants/status');
const uuid = require('uuid');
module.exports = {
    getAllCandidate: getAllCandidate,
    addCandidate: addCandidate,
    deleteCandidate: deleteCandidate,
    updateCandidate: updateCandidate,
    updateImageCandidate: updateImageCandidate,
}
function getAllCandidate(){
    return candidate.find()
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function addCandidate(body){
    let newCandidate = new candidate(body)
    return newCandidate.save()
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function deleteCandidate(id){
    return candidate.findByIdAndRemove(id)
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function updateCandidate(id,data){
    return candidate.findByIdAndUpdate({_id:id},data)
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function updateImageCandidate(id,file){
    return candidate.findOne({_id: id})
    .then((candidate) =>{
        if(candidate){
            return new Promise((resolve,reject) =>{
                var img = 'candidate_' + uuid.v4() + '.png';
                candidate.image.push(img)
                file.mv(path.join(__dirname, '../public/image/' + img), (err) =>{
                    if(err){
                        return reject(err);
                    }
                    return candidate.update({_id:id},{$set :{image: candidate.image}})
                    .then((data)=>{
                        return resolve(data);
                    })
                    .catch((err)=>{
                        return reject(err);
                    })
                })
            });
        }else{
            return Promise.reject({
                message: "not Found"
            });
        }
    }).catch((err)=>{
        return Promise.reject(err);
    })
}