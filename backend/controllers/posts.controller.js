const path = require('path');
const Status = require('../constants/status');
const uuid = require('uuid');
const post = require('../models/posts.model');
module.exports ={
    getAllPost: getAllPost,
    createPost: createPost,
    deletePost: deletePost,
    updatePost: updatePost,
    updateImagePost: updateImagePost
}
function getAllPost(){
    return post.find()
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function createPost(body){
    var newPost = new post(body);
    return newPost.save()
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function deletePost(id){
    return post.findByIdAndRemove(id)
    .then((res)=>{
        return Promise.resolve(res);
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function updatePost(id,data){
    return post.findOneAndUpdate({_id:id}, data)
    .then((res)=>{
        return Promise.resolve(res)
    })
    .catch((err)=>{
        return Promise.reject(err);
    })
}
function updateImagePost(id,file){
    return post.findOne({_id: id})
    .then((post) =>{
        if(post){
            return new Promise((resolve,reject) =>{
                var img = 'candidate_' + uuid.v4() + '.png';
                post.image.push(img)
                file.mv(path.join(__dirname, '../public/image/' + img), (err) =>{
                    if(err){
                        return reject(err);
                    }
                    return post.update({_id:id},{$set :{image: post.image}})
                    .then((data)=>{
                        return resolve(data);
                    })
                    .catch((err)=>{
                        return reject(err);
                    })
                })
            });
        }else{
            return Promise.reject({
                message: "not Found"
            });
        }
    }).catch((err)=>{
        return Promise.reject(err);
    })
}
